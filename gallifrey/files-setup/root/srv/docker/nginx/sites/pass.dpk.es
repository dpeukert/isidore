server {
	include /etc/nginx/listen.conf;
	server_name pass.dpk.es;

	include /etc/nginx/no-ai.conf;

	location / {
		proxy_pass http://bitwarden:80;
	}

	location = /.well-known/change-password {
		return 302 https://$host/#/settings/account;
	}

	include /etc/nginx/securitytxt.conf;
	include /etc/nginx/robotstxt-disallow.conf;
}
