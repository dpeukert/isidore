server {
	include /etc/nginx/listen.conf;
	server_name autoconfig.peukert.cc;

	include /etc/nginx/no-ai.conf;

	location = /mail/config-v1.1.xml {
		default_type 'text/xml';
		return 200 '<?xml version="1.0" encoding="UTF-8"?>
<clientConfig version="1.1">
	<emailProvider id="peukert.cc">
		<domain>peukert.cc</domain>
		<displayName>peukert.cc</displayName>
		<displayShortName>peukert.cc</displayShortName>
		<incomingServer type="imap">
			<hostname>imap.migadu.com</hostname>
			<port>993</port>
			<socketType>SSL</socketType>
			<username>%EMAILADDRESS%</username>
			<authentication>password-cleartext</authentication>
		</incomingServer>
		<incomingServer type="pop3">
			<hostname>pop.migadu.com</hostname>
			<port>995</port>
			<socketType>SSL</socketType>
			<username>%EMAILADDRESS%</username>
			<authentication>password-cleartext</authentication>
		</incomingServer>
		<outgoingServer type="smtp">
			<hostname>smtp.migadu.com</hostname>
			<port>465</port>
			<socketType>SSL</socketType>
			<username>%EMAILADDRESS%</username>
			<authentication>password-cleartext</authentication>
		</outgoingServer>
		<outgoingServer type="smtp">
			<hostname>smtp.migadu.com</hostname>
			<port>587</port>
			<socketType>STARTTLS</socketType>
			<username>%EMAILADDRESS%</username>
			<authentication>password-cleartext</authentication>
		</outgoingServer>
		<documentation url="https://www.migadu.com/guides/#generic-settings">
			<descr lang="en">Generic Settings</descr>
		</documentation>
	</emailProvider>
	<webMail>
		<loginPage url="https://webmail.migadu.com/" />
	</webMail>
	<clientConfigUpdate url="https://autoconfig.peukert.cc/mail/config-v1.1.xml" />
</clientConfig>
';
	}

	location / {
		return 404;
	}

	include /etc/nginx/securitytxt.conf;
	include /etc/nginx/robotstxt-disallow.conf;
}
