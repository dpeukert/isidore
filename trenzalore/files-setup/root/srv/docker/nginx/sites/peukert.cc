server {
	include /etc/nginx/listen.conf;
	server_name peukert.cc;

	include /etc/nginx/no-ai.conf;

	location = /.well-known/carddav {
		return 301 https://cloud.dpk.es/remote.php/dav;
	}

	location = /.well-known/caldav {
		return 301 https://cloud.dpk.es/remote.php/dav;
	}

	location / {
		return 404;
	}

	include /etc/nginx/securitytxt.conf;
	include /etc/nginx/robotstxt-disallow.conf;
}
