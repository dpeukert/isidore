#!/bin/bash

log 'CONFIGURING SERVICES'
	sudo systemctl enable --now 'pcscd.socket'
checkmark

log 'SETTING UP ARDUINO'
	arduino-cli config init
	arduino-cli core update-index
	arduino-cli core install arduino:avr
checkmark

log 'ADDING TODO NOTES'
	cat << EOF >> "/home/$device/TODO.md"

## Steam
- login
- configure
- install games
  - restore saves and configs if needed

## TeamSpeak 3
- add nickname to identity
- add servers

## Discord
- login
- configure

## Openplanet
\`\`\`bash
# Download OP from openplanet.dev
wget 'https://aka.ms/vs/17/release/vc_redist.x64.exe'

export WINEPREFIX="\$HOME/.local/share/Steam/steamapps/compatdata/2225070/pfx"
export WINEPATH="\$HOME/.local/share/Steam/steamapps/common/Proton - Experimental/dist/bin/wine64"

wine 'VC_redist.x64.exe'
wine 'OpenplanetNext_'*'.exe' # Use Z:\home\$USER\.local\share\Steam\steamapps\common\Trackmania\ for install path
\`\`\`

### Plugins
Use the in-game Plugin Manager to install the following plugins:
- [Checkpoint Counter by Phlarx](https://openplanet.dev/plugin/checkpointcounter/)
- [Drive Alone (No Ghost Selection) by xPaw](https://openplanet.dev/plugin/drivealone/)
- [Load progress by Miss](https://openplanet.dev/plugin/loadprogress/)
- [Speedometer by Greep](https://openplanet.dev/plugin/speedometer/)
- [Split Speeds by RuteNL](https://openplanet.dev/plugin/splitspeeds/)
- [Ultimate Medals by Phlarx](https://openplanet.dev/plugin/ultimatemedals/)
- [Ultrawide UI Fix by me](https://openplanet.dev/plugin/ultrawideuifix/)
EOF
checkmark
