#!/bin/bash

# Check that we are running on a UEFI system
if [ ! -d '/sys/firmware/efi' ]; then
	abort 1 'This script can only be run on a UEFI system, aborting...'
fi

# Check that we are running on Arch just to be sure
if ! grep -q 'archlinux' '/proc/version'; then
	abort 1 'This script can only be run on Arch Linux, aborting...'
fi

# Check that we have an internet connection as we need to download packages
if ! ping -c 1 "$ping_ip" > '/dev/null' 2>&1; then
	abort 1 'Network connection not available, aborting...'
fi

log 'TEMPORARILY SETTING CONSOLE KEYMAP AND FONT'
	loadkeys "$keymap"
	setfont "$font"
checkmark

# Get the information we need from the user
drives="$(lsblk -ndo NAME,SIZE)"
drives="$(sed -n '/^loop/!s|^|/dev/|p' <<< "${drives}")"
drives="$(sort <<< "${drives}")"
mapfile -t drives <<< "${drives}"
inputselect 'Drive' "${drives[@]}"
drive="$(echo "$VALUE" | cut -d " " -f 1)"

inputprompt 'string' 'Full name' 'true'
full_name="$VALUE"

inputprompt 'string' 'Password'
password="$VALUE"

inputprompt 'string' 'Password again'
password_again="$VALUE"

while [ "$password" != "$password_again" ]; do
	echo 'Passwords do not match.'
	inputprompt 'string' 'Password again'
	password_again="$VALUE"
done

log 'PARTITIONING DRIVE'
	partition_prefix="$drive"
	if [ "$(printf '%s' "$drive" | tail -c 1)" -ge 0 ] 2> '/dev/null'; then
		partition_prefix="${partition_prefix}p"
	fi
	sgdisk --zap-all "$drive"
	sgdisk --clear "$drive"
	sgdisk -n 1:0:+512MiB -t 1:EF00 "$drive"
	sgdisk -n 2:0:0 -t 2:8300 "$drive"
	mkfs.fat -F32 "${partition_prefix}1"
	mkfs.ext4 "${partition_prefix}2"
	mount "${partition_prefix}2" "$arch_root"
	mkdir -p "$arch_root/boot/"
	mount "${partition_prefix}1" "$arch_root/boot"
checkmark

log 'DOWNLOADING MIRRORS'
	curl --location --silent --fail "https://archlinux.org/mirrorlist/?country=$country&use_mirror_status=on" | sed 's/^#Server/Server/' > '/etc/pacman.d/mirrorlist'
	chmod +r '/etc/pacman.d/mirrorlist'
checkmark

log 'INSTALLING BASE PACKAGES'
	updatemakepkgconf
	pacstrap -C "$dir/_tags/archlinux-base/files-install/root/etc/pacman.conf" "$arch_root" base base-devel efibootmgr linux linux-firmware inetutils nano iwd python dbus-python ncurses python-gobject
checkmark

log 'GENERATING FSTAB'
	genfstab -U "$arch_root" > "$arch_root/etc/fstab"
checkmark

log 'SETTING KEYMAP, FONT & LOCALE'
	printf 'KEYMAP=%s\nFONT=%s\n' "$keymap" "$font" > "$arch_root/etc/vconsole.conf"
	echo 'LANG=en_GB.UTF-8' > "$arch_root/etc/locale.conf"
	printf 'en_GB.UTF-8 UTF-8\nen_US.UTF-8 UTF-8\ncs_CZ.UTF-8 UTF-8\n' > "$arch_root/etc/locale.gen"
	runinchroot 'locale-gen'
checkmark

log 'SETTING TIMEZONE'
	ln -sf "/usr/share/zoneinfo/$timezone" "$arch_root/etc/localtime"
checkmark

log 'SETTING HOSTNAME'
	echo "$device" > "$arch_root/etc/hostname"
	printf '127.0.0.1 localhost\n::1 localhost\n127.0.1.1 %s.localdomain %s\n' "$device" "$device" > "$arch_root/etc/hosts"
checkmark

log 'SETTING UP SUDOERS CONFIG'
	echo 'Defaults passwd_timeout=0, insults' > "$arch_root/etc/sudoers.d/00-defaults"
	echo '%wheel ALL=(ALL) ALL' > "$arch_root/etc/sudoers.d/10-wheel"
	echo '%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl poweroff,/usr/bin/systemctl reboot,/usr/bin/systemctl suspend' > "$arch_root/etc/sudoers.d/20-power"
checkmark

log 'SETTING UP MAKEPKG CONFIG'
	updatemakepkgconf "$arch_root"
checkmark

log 'SETTING UP REGULAR USER & DISABLING ROOT LOGIN'
	runinchroot "useradd -m -g users -G wheel,uucp,video -s '/bin/bash' '$device'"
	runinchroot "echo '$device:$password' | chpasswd"
	if [ -n "$full_name" ]; then
		runinchroot "chfn -f '$full_name' '$device'"
	fi
	runinchroot "passwd --lock 'root'"
checkmark


log 'ADDING UEFI BOOT ENTRY'
	# Prepare kernel parameter string from list files
	if [ -f "$dir/_lists/kernel.list" ]; then
		kernel_parameter_list="$(sed '/^#/ d' "$dir/_lists/kernel.list" | tr '\n' ' ')"
		kernel_parameter_list=" $(trimstring "$kernel_parameter_list")"
	else
		kernel_parameter_list=' '
	fi

	# Add boot entry
	runinchroot "efibootmgr -d '$drive' -p 1 -c -L 'Arch Linux' -l '/vmlinuz-linux' -u 'root=PARTUUID=$(blkid -s PARTUUID -o value "${partition_prefix}2") rw$kernel_parameter_list initrd=\initramfs-linux.img'"
checkmark
