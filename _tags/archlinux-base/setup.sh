#!/bin/bash

log 'SETTING UP INTERNET'
	sudo systemctl enable --now 'systemd-networkd.service'
	sudo systemctl enable --now 'systemd-resolved.service'
	if ls '/sys/class/net/wl'* > '/dev/null' 2>&1; then
		wifi=1
		sudo systemctl enable --now 'iwd.service'
	else
		wifi=0
	fi
checkmark

# Wait for the network to be set up
sleep 10

log 'SETTING UP RESOLV.CONF SYMLINK'
	sudo ln -rsf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
checkmark

# If we don't have an internet connection and we have WiFi, run the WiFi menu
if ! ping -c 1 "$ping_ip" > '/dev/null' 2>&1 && [ "$wifi" = '1' ]; then
	sudo chmod +x '/usr/local/bin/iwd-menu'
	iwd-menu
fi

log 'CHECKING INTERNET'
	while ! ping -c 1 "$ping_ip"; do sleep 1; done
checkmark

log 'UPDATING DATABASES AND INSTALLED PACKAGES'
	sudo pacman -Syyu --noconfirm
checkmark

log 'INSTALLING TRIZEN'
	temp_dir="$(mktemp -d)"
	curl --location --silent --fail 'https://aur.archlinux.org/cgit/aur.git/snapshot/trizen.tar.gz' | tar -xzC "$temp_dir" --strip-components 1 -f -
	cd "$temp_dir"
	makepkg -sri --noconfirm
	cd "$OLDPWD"
	rm -rf "$temp_dir"

	# This causes trizen to generate its config
	trizen --version > '/dev/null'

	# Set the trizen clone_dir to /tmp
	sed 's|clone_dir\s*=>\s*"[^"]*"|clone_dir => "/tmp/trizen-$ENV{USER}"|' -i "/home/$device/.config/trizen/trizen.conf"
checkmark

log 'INSTALLING REPO PACKAGES'
	if [ -f "$dir/_lists/repo.list" ]; then
		installpackages "$dir/_lists/repo.list"
	fi
checkmark

log 'INSTALLING AUR PACKAGES'
	if [ -f "$dir/_lists/aur.list" ]; then
		installpackages "$dir/_lists/aur.list"
	fi
checkmark
