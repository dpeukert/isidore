## Restore from backup
- Chromium
- Firefox
- Thunderbird

## Gnome Keyring
- set blank password for keyring

## Login
- Tidal Hifi
- Messenger
- Slack

## Reenable autostart apps
- uncomment execs in `~/.config/sway/config`
