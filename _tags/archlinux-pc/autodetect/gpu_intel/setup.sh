#!/bin/bash

flags='AcceleratedVideoDecodeLinuxGL'
sed --regexp-extended "s/^(--enable-features=.*)$/\1,${flags}/" -i "${HOME}/.config/chromium-flags.conf"
sed --regexp-extended "s/^(--enable-features=.*)$/\1,${flags}/" -i "${HOME}/.config/electron-flags.conf"
