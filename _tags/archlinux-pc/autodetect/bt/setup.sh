#!/bin/bash

log 'CONFIGURING BLUETOOTH'
	sudo systemctl enable --now 'bluetooth.service'

	while ! bluetoothctl system-alias "$device"; do
		echo 'Unable to set Bluetooth alias, trying again in 10 seconds'
		sleep 10
	done

	bluetoothctl power on
	bluetoothctl pairable on
	bluetoothctl discoverable off
	bluetoothctl discoverable-timeout 300
checkmark
