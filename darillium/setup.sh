#!/bin/vbash
# shellcheck shell=bash disable=SC2121,SC2312

log 'CONFIGURING USERS'
	set system login user "${device:?}" authentication plaintext-password "${PASSWORD:?}"
	set system login user "${device:?}" authentication public-keys id_ed25519 type "$(cut --delimiter ' ' --fields 1 "${dir?:}/_tags/archlinux-pc/files-setup/user/.ssh/id_ed25519.pub")"
	set system login user "${device:?}" authentication public-keys id_ed25519 key "$(cut --delimiter ' ' --fields 2 "${dir?:}/_tags/archlinux-pc/files-setup/user/.ssh/id_ed25519.pub")"
	delete system login user vyos
checkmark

log 'CONFIGURING SYSTEM SETTINGS'
	set system host-name "${device:?}"
	set system name-server '127.0.0.1'
	set system update-check url 'https://raw.githubusercontent.com/vyos/vyos-nightly-build/refs/heads/current/version.json'
	delete system console
	delete service ntp server 'time1.vyos.net'
	delete service ntp server 'time2.vyos.net'
	delete service ntp server 'time3.vyos.net'
	set service ntp server '0.pool.ntp.org'
	set service ntp server '1.pool.ntp.org'
	set service ntp server '2.pool.ntp.org'
	set service ntp server '3.pool.ntp.org'
	set system time-zone "${timezone:?}"
checkmark

log 'CONFIGURING INTERFACES'
	set system ipv6 disable-forwarding

	set interfaces ethernet eth0 description 'LAN'
	set interfaces ethernet eth0 address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 1)/${LAN_NETWORK_MASK:?}"
	delete interfaces ethernet eth0 offload

	set interfaces ethernet eth1 description 'WAN'
	set interfaces ethernet eth1 address 'dhcp'
	delete interfaces ethernet eth1 offload

	set interfaces ethernet eth2 disable
	delete interfaces ethernet eth2 offload

	set interfaces ethernet eth3 disable
	delete interfaces ethernet eth3 offload
checkmark

log 'CONFIGURING FIREWALL'
	# Flowtables
	set firewall flowtable FT1 description 'Flow table for the forward chain'
	set firewall flowtable FT1 offload 'software'
	set firewall flowtable FT1 interface 'eth0'
	set firewall flowtable FT1 interface 'eth1'

	# Input
	set firewall ipv4 input filter default-action 'drop'

	set firewall ipv4 input filter rule 10 description 'Allow return traffic destined to the router'
	set firewall ipv4 input filter rule 10 action 'accept'
	set firewall ipv4 input filter rule 10 inbound-interface name 'eth1'
	set firewall ipv4 input filter rule 10 state 'established'
	set firewall ipv4 input filter rule 10 state 'related'

	set firewall ipv4 input filter rule 20 description 'Allow ICMP traffic from WAN'
	set firewall ipv4 input filter rule 20 action 'accept'
	set firewall ipv4 input filter rule 20 inbound-interface name 'eth1'
	set firewall ipv4 input filter rule 20 protocol 'icmp'
	set firewall ipv4 input filter rule 20 state 'new'

	set firewall ipv4 input filter rule 30 description 'Allow DNS traffic from WAN'
	set firewall ipv4 input filter rule 30 action 'accept'
	set firewall ipv4 input filter rule 30 inbound-interface name 'eth1'
	set firewall ipv4 input filter rule 30 protocol 'tcp_udp'

	set firewall ipv4 input filter rule 1000 description 'Allow all traffic from LAN'
	set firewall ipv4 input filter rule 1000 inbound-interface name 'eth0'
	set firewall ipv4 input filter rule 1000 action 'accept'

	set firewall ipv4 input filter rule 2000 description 'Allow all traffic from localhost'
	set firewall ipv4 input filter rule 2000 inbound-interface name 'lo'
	set firewall ipv4 input filter rule 2000 action 'accept'

	# Output
	set firewall ipv4 output filter default-action 'accept'

	# Forward
	set firewall ipv4 forward filter default-action 'drop'

	set firewall ipv4 forward filter rule 10 description 'Allow return traffic through the router - fastpath'
	set firewall ipv4 forward filter rule 10 action 'offload'
	set firewall ipv4 forward filter rule 10 offload-target 'FT1'
	set firewall ipv4 forward filter rule 10 state 'established'
	set firewall ipv4 forward filter rule 10 state 'related'

	set firewall ipv4 forward filter rule 20 description 'Allow return traffic through the router'
	set firewall ipv4 forward filter rule 20 action 'accept'
	set firewall ipv4 forward filter rule 20 inbound-interface name 'eth1'
	set firewall ipv4 forward filter rule 20 state 'established'
	set firewall ipv4 forward filter rule 20 state 'related'

	set firewall ipv4 forward filter rule 30 description 'Allow traffic forwarded via NAT'
	set firewall ipv4 forward filter rule 30 action 'accept'
	set firewall ipv4 forward filter rule 30 connection-status nat 'destination'
	set firewall ipv4 forward filter rule 30 state 'new'

	set firewall ipv4 forward filter rule 1000 description 'Allow all traffic from LAN'
	set firewall ipv4 forward filter rule 1000 action 'accept'
	set firewall ipv4 forward filter rule 1000 inbound-interface name 'eth0'
checkmark

log 'CONFIGURING NAT'
	# Destination NAT
	set nat destination rule 10 description 'Forward HTTP traffic to gallifrey'
	set nat destination rule 10 inbound-interface name 'eth1'
	set nat destination rule 10 protocol 'tcp_udp'
	set nat destination rule 10 destination port '80'
	set nat destination rule 10 translation address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 3)"

	set nat destination rule 20 description 'Forward HTTPS traffic to gallifrey'
	set nat destination rule 20 inbound-interface name 'eth1'
	set nat destination rule 20 protocol 'tcp_udp'
	set nat destination rule 20 destination port '443'
	set nat destination rule 20 translation address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 3)"

	set nat destination rule 30 description 'Forward SSH traffic to gallifrey'
	set nat destination rule 30 inbound-interface name 'eth1'
	set nat destination rule 30 protocol 'tcp_udp'
	set nat destination rule 30 destination port '22'
	set nat destination rule 30 translation address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 3)"

	# Source NAT
	set nat source rule 10 description 'NAT source address for all traffic to WAN'
	set nat source rule 10 outbound-interface name 'eth1'
	set nat source rule 10 translation address 'masquerade'
checkmark

log 'CONFIGURING DHCP SERVER'
	set service dhcp-server shared-network-name "${device:?}-pool" authoritative
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" subnet-id '1'
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" option default-router "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 1)"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" option name-server "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 1)"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" range 0 start "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 10)"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" range 0 stop "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 254)"

	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" static-mapping karn ip-address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 2)"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" static-mapping karn mac "${KARN_MAC:?}"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" static-mapping gallifrey ip-address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 3)"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" static-mapping gallifrey mac "${GALLIFREY_MAC:?}"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" static-mapping mars ip-address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 4)"
	set service dhcp-server shared-network-name "${device:?}-pool" subnet "${LAN_NETWORK_PREFIX:?}/${LAN_NETWORK_MASK:?}" static-mapping mars mac "${MARS_MAC:?}"
checkmark

log 'CONFIGURING DNS SERVER'
	set container name bind host-name 'bind'
	set container name bind image 'internetsystemsconsortium/bind9:9.20'
	set container name bind arguments '-4 -g -c /etc/bind/named.conf'
	set container name bind allow-host-networks
	set container name bind restart 'always'
	set container name bind cpu-quota '0'
	set container name bind memory '0'
	set container name bind volume bind-cache source '/config/user-data/bind-cache/'
	set container name bind volume bind-cache destination '/var/cache/bind/'
	set container name bind volume bind-config source '/config/user-data/bind-config/'
	set container name bind volume bind-config destination '/etc/bind/'
	mkdir --parents '/config/user-data/bind-cache/' '/config/user-data/bind-config/'
	sudo chown -R 53:53 '/config/user-data/bind-cache/' '/config/user-data/bind-config/'
	# TODO: copy config files
	write_bind_config '/config/user-data/bind-config/named.conf.in'
	write_internal_zone_file '/config/user-data/bind-config/dpk.es.zone.in'
	write_external_zone_file '/config/user-data/bind-config/dpk.es.zone.in'
	# TODO: copy keys
	# TODO: copy config files
	# TODO: copy update-bind-configs.sh and util.sh
checkmark

log 'PREPARING LOCAL DNS CONFIG UPDATE SCRIPT'
	cp "${dir}/_platforms/vyos/util.sh" '/config/user-data/util.sh'
	cp "${dir}/_platforms/vyos/update-bind-config.sh" '/config/user-data/update-bind-config.sh'
checkmark

log 'CONFIGURING SSH SERVER'
	set service ssh listen-address "$(nth_in_prefix "${LAN_NETWORK_PREFIX:?}" 1)"
	set service ssh disable-password-authentication
checkmark
