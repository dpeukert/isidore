#!/bin/bash

# Set shell options
set -e -u -o 'pipefail'
shopt -s 'nullglob'

if [[ -z "${1:-}" ]]; then
	echo 'Script root path not provided, aborting...'
	exit 1
fi

if [[ -z "${2:-}" ]] || [[ ! -f "$1/$2/setup.rsc" ]]; then
	echo 'Device name not provided or device script does not exist, aborting...'
	exit 1
fi

dir="$1"
device="$2"

# shellcheck source=../../util.sh
. "${dir}/util.sh"

output_dir="${dir}/output"
output_path="${output_dir}/output.rsc"

if [[ -d "${output_dir}" ]]; then
	rm -rf "${output_dir}"
fi

mkdir -p "${output_dir}/"

timestamp="$(date +%c)"
echo "# Generated on ${timestamp} by ${USER}@${HOSTNAME}" > "${output_path}"

declare -A answers
config_section=0

exec 3<"${dir}/${device}/setup.rsc"

while IFS='' read -r -u 3 line || [[ -n "${line}" ]]; do
	if [[ "${line}" = '-----BEGIN CONFIG-----' ]]; then
		config_section=1
		device_name_bytes="$(stringtobytes "${device}")"
		echo ":local hostname \"${device_name_bytes}\";" >> "${output_path}"
	elif [[ "${line}" = '-----END CONFIG-----' ]]; then
		config_section=0
	elif [[ "${config_section}" = 1 ]]; then
		if printf '%s' "${line}" | grep -q '^#'; then	continue; fi

		IFS=';' read -ra PARAMS <<< "${line}"

		if [[ "${#PARAMS[@]}" -ne 5 ]]; then
			abort 1 "Config line \"${line}\" is malformed, aborting..."
		fi

		name="$(trimstring "${PARAMS[0]}")"
		type="$(trimstring "${PARAMS[1]}")"
		prompt="$(trimstring "${PARAMS[2]}")"
		conditional_variable="$(trimstring "${PARAMS[3]}")"
		conditional_value="$(trimstring "${PARAMS[4]}")"

		if [[ "${name}" = '' ]]; then
			abort 1 'Variable name must be a non-empty string, aborting...'
		fi

		# shellcheck disable=SC2310 # We act based on the return value of isvalidprompttype
		if ! isvalidprompttype "${type}"; then
			abort 1 'Variable type must be one of string, bytestring, bool, ip, ipprefix or mac, aborting...'
		fi

		if [[ "${prompt}" = '' ]]; then
			abort 1 'Prompt must be a non-empty string, aborting...'
		fi

		if [[ "${conditional_variable}" != 'false' ]]; then
			if [[ "${conditional_variable}" = '' ]] || [[ -z "${answers[${conditional_variable}]}" ]]; then
				abort 1 'Conditional prompt variable must be false or a valid variable name, aborting...'
			fi

			if [[ "${conditional_value}" = '' ]]; then
				abort 1 'Conditional prompt value must be false or a non-empty string, aborting...'
			fi
		fi

		if [[ "${conditional_variable}" = 'false' ]] || [[ "${answers[${conditional_variable}]}" = "${conditional_value}" ]]; then
			inputprompt "${type}" "${prompt}"
			answers[${name}]="${VALUE}"

			printf ':local %s ' "${name}" >> "${output_path}"
			if [[ "${type}" = 'string' ]] || [[ "${type}" = 'bytestring' ]] || [[ "${type}" = 'mac' ]]; then
				printf '"%s"' "${VALUE}" >> "${output_path}"
			else
				printf '%s' "${VALUE}" >> "${output_path}"
			fi
			printf ';\n' >> "${output_path}"
		fi
	else
		echo "${line}" >> "${output_path}"
	fi
done

inputprompt 'string' 'Provide a path to the public SSH key you want to use'

if [[ ! -f "${VALUE}" ]]; then
	abort 1 "The provided path doesn't exist, aborting..."
fi

cp "${VALUE}" "${output_dir}/ssh.pub"
